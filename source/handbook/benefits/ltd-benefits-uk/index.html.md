---
layout: markdown_page
title: "GitLab LTD (UK) Benefits"
---

Can't find what you're looking for? Try the main [People Operations page](/handbook/people-operations).

## On this page
{:.no_toc}

- TOC
{:toc}

----

## Specific to UK based employees

### Medical Insurance

For UK based employees GitLab provides paid membership of medical insurance with AXA PPP. Family members can be added to the insurance and GitLab will pay 50% of the cost to include them. Further information can also be found in the [AXA PPP Brochure](https://drive.google.com/a/gitlab.com/file/d/0Bwy71gCp1WgtUXcxeFBaM0MyT00/view?usp=sharing). Please let People Ops know if you would like to join the scheme. Please also note that this is a taxable benefit.

### Pension

GitLab will be providing a workplace pension for all UK employees. The process of selecting a pension plan and a provider has been started and you can find out more details on that here: https://gitlab.com/gitlab-com/peopleops/issues/340. Once the plan has been setup this section will be updated.
