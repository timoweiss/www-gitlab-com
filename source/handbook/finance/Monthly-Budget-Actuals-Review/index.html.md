---
layout: markdown_page
title: "Monthly Budget Variance Analysis"
---

## Monthly Budget Variance Analysis

#### Purpose
Each month after the financials have been published, we review department spend data in detail. The goal of this analysis is to compare department budgets with actual results and examine any material discrepancies between budgeted and actual costs. These costs are reviewed at the department level, allowing us to measure progress in meeting our plan, forecast, and operating model. 
#### Process
Following the month-end close, the Accounting Manager distributes department income statements to the appropriate team members. Each department is then responsible for comparing these reports, which contain actual costs, to the budget spreadsheet in Google drive. Departments should analyze their data and if necessary, discuss items of interest and take appropriate action. Any questions regarding the cost data should be sent to the Accounting Manager. 
#### Timing
The Accounting Manager will send the income statements on or before the 15th of each month. Departments should perform their analysis and make any related inquiries within a day or two of receiving the reports. 
