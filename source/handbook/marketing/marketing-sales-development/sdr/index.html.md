---
layout: markdown_page
title: "Sales Development"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Introduction

As a BDR (Business Development Representative) or SDR (Sales Development Representative) your focus will be on generating opportunities that the AEs (Account Executives) and SALs (Strategic Account Leaders) accept, ultimately leading to closed won business. On this team we work hard, but have fun too (I know, it's a cliche ...but here it's true!). We will work hard to guarantee your success if you do the same. We value results, transparency, sharing, freedom, efficiency, frugality, collaboration, directness, kindness, diversity, solutions, and quirkiness.

Being a BDR or SDR can come with what seems like long days, hard work, frustration, and even at times, failure. If you have the right mindset and come to work with a tenacious, hungry attitude you will see growth personally and in your career. GitLab is a place with endless opportunity. Let’s make it happen!

## Your Role

As a BDR or SDR, you will be dealing with the front end of the sales process. You play a crucial role that helps bridge the gap between sales and marketing. You will be tasked with generating sales accepted opportunities for GitLab.

As you gain knowledge, you will be able to aid our future customers ship better software, faster. There are numerous resources at your fingertips that we have created to help you in this process. You have:

1. Handbook - [Inbound BDR](/handbook/marketing/marketing-sales-development/sdr/inbound) or [Outbound SDR](/handbook/marketing/marketing-sales-development/sdr/outbound) - This will help you with your day to day workflow. You can find information on how to prospect, best practices, customer FAQs, buyer types, cadence samples and more. Use the handbook in conjunction with the [Marketing](/handbook/marketing/), [Sales](/handbook/sales/) and [Revenue OPS](/handbook/revenue-ops/) handbooks. This will help you bridge the gap between the two and learn the product and process faster.
2. [GLU GitLab University](https://docs.gitlab.com/ce/university/) - These trainings will teach the fundamentals of Version Control with Git and GitLab through courses that cover topics which can be mastered in about 2 hours. These trainings include an intro to Git, GitLab basics, a demo of Gitlab.com, terminology, and more. You can also find information about GitLab compared to other tools in the market.
3. Gmail
4. Salesforce
5. [Outreach](https://outreach.io/)
6. LinkedIn
7. DiscoverOrg (Outbound)
8. [Grovo](https://www.grovo.com/) - A learning management system with short, engaging tracks focused on specific skills.
9. Slack
10. Drift (Inbound)
11. Google Drive

## Criteria for Sales Accepted Opportunity (SAO)<a name="acceptedopp"></a>

* Account is a Mid Market, Large or Strategic account (SDRs may only get credit for SAOs in Large or Strategic accounts)
* Right person/right profile - can influence a purchase of GitLab as an evaluator, decision maker, technical buyer, or influencer.
* High level business need that GitLab can address
* Prospects wants to learn more about how GitLab can address their stated business need
* The prospect has committed to a next call to go deeper into their current situation and needs
* An opportunity should immediately be moved into stage 1 - Discovery if it meets the above criteria
* If user count is determined after the initial discovery call, the Account Executive should update the Opportunity name accordingly

## Additional Resources

- [GitLab Primer](https://about.gitlab.com/primer/)
- [Glossary of Terms](https://docs.gitlab.com/ce/university/glossary/README.html)
- [GitLab Positioning](https://about.gitlab.com/handbook/positioning-faq/)
- [EE Product Qualification Questions](https://about.gitlab.com/handbook/EE-Product-Qualification-Questions/)
- [Sales Qualification Questions](https://about.gitlab.com/handbook/sales-qualification-questions/)
- [FAQ from Prospects](https://about.gitlab.com/handbook/sales-faq-from-prospects/)
- [Customer Use Cases](https://about.gitlab.com/handbook/use-cases/)
- [GitLab University](https://docs.gitlab.com/ce/university/)
- [Platzi GitLab Workshop](https://courses.platzi.com/classes/git-gitlab/)
- [GitLab Market and Ecosystem](https://www.youtube.com/watch?v=sXlhgPK1NTY&list=PLFGfElNsQthbQu_IWlNOxul0TbS_2JH-e&index=6)
- [GitLab Documentation](http://docs.gitlab.com/ee/)
- [GitLab CI/CD Demo](https://about.gitlab.com/2017/03/13/ci-cd-demo/)
- [GitLab Compared to Other Tools](https://about.gitlab.com/comparison/)
- [GitLab Battlecards](https://docs.google.com/document/d/1zRIvk4CaF3FtfLfSK2iNWsG-znlh64GNeeMwrTmia_g/edit#)
- [How to Use Outreach](https://docs.google.com/document/d/1FzvGEsL6ukxFZtk7ZkMUVAAT_wW-aVblAu1yOFWiEGo/edit)
- [Outreach University](http://university.outreach.io/)
- [Version.gitlab](https://version.gitlab.com/users/sign_in)
- [Resellers Handbook](https://about.gitlab.com/handbook/resellers/)
- [Customer Reference Sheet](https://docs.google.com/a/gitlab.com/spreadsheets/d/1Off9pVkc2krT90TyOEevmr4ZtTEmutMj8dLgCnIbhRs/edit?usp=sharing)
- [Support Handbook](https://about.gitlab.com/handbook/support/)
- [GitHost](https://about.gitlab.com/gitlab-hosted/)
- [Additional Training Resources](https://drive.google.com/drive/folders/0B1W9hTDXggO1NGJwMS12R09lakE)

## Asking questions

Don't hesitate to ping one of your colleagues with a question, or someone on the team who specializes in what you're searching for. Everyone is happy to help, and it's better to get your work done faster with your team, than being held up at a road block.

## Who to go to for questions about

### Salesforce

- #sfdc-users
- Francis Aquino

### Lead Questions or Issues

- #lead-questions
- JJ Cordz
- Molly Young

### Technical questions about GitLab

- #support
- Assigned buddy
- BDR Team Lead
- SDR Team Lead 

### Outreach

- #outreach
- Chet Backman
- Francis Aquino

### Resellers

- Michael Alessio
- Jim Torres
- BDR Team Lead

### Partnerships

- Eliran Mesika

## What are my work hours?

The handbook says “You should have clear objectives and the freedom to work on them as you see fit. Any instructions are open to discussion. You don’t have to defend how you spend your day. We trust team members to do the right thing instead of having rigid rules”.

## Who should I connect with for my 10 1:1 onboarding calls?

You can connect with whomever you please. Check out the [Team Page](https://about.gitlab.com/team/) for ideas. Though you will be working most closely with the Sales and Marketing teams, it is encouraged you get to know people all around the organization.

## Improvements/Contributing

- If you get an answer to a question someone else may benefit from, incorporate it into this handbook or add it to the FAQ document
- After meetings or process changes, feel free to update this handbook, and submit a merge request.
- Create issues for any idea (small or large that), that you want feedback on
- All issued and MRs for changes to the SDR handbook assign to Chet Backman
