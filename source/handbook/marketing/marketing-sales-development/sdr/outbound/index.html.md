---
layout: markdown_page
title: "Outbound SDR"
---
---


# Outbound SDR Handbook<a name="outbound"></a>



## Table of Contents
- [Your Role](#role)
- [Working with Account Executives](#salessupport)
- [Who to call](#distribution)
- [Compensation](#compensation)
- [Other measurements](#measurements)
- [Other expectations](#expectations)

### Your Role<a name="role"></a>

As an Outbound Sales Development Representative (SDR), you will be responsible for one of the most difficult and important parts of growing a business: outbound prospecting. You play a crucial role that helps bridge the gap between sales and marketing. You will be tasked with generating sales accepted opportunities, within our large and strategic account segments.

### Working with Account Executives<a name="salessupport"></a>

Account Executives (AEs) receive support from the Outbound SDR team. Meeting cadence consist of the following:

- **First kick-off meeting**: Time - 1hour; Discuss strategy, accounts and schedules
- **Monthly Recurring Strategy Meeting**: Time - 1hour; Evalutate strategy and opportunities
- **Weekly (or biweekly) Status Meeting**: Time - 30 minutes; Discuss initial meetings and opportunities

Additional ad-hoc meetings may be scheduled in addition to the above.

AEs have direct impact on the Outbound SDR's success. Forming a strong relationship with the AEs and SALs you support, and acting as a single integrated account team will play a crucial role in obtaining quota each month. The AEs and SALs you work with will know a lot about the accounts you are prospecting into, so be sure to be in regular communication to ensure you are as effective as possible.

### Who to call<a name="distribution"></a>

* Your SDR Team Lead will assign ~25-30 accounts per quarter in Salesforce from in the Strategic and Large segments. These target accounts will be provided by the SDR Team Lead, in close partnership with the relevant Regional Sales Director. The accounts will be identified and selected as the best accounts to focus on in order to meet our revenue objectives. The SDR Team Lead or Sales & Business Development Manager will add you to the SDR Field in Salesforce.
* If you inherit an account that was previously worked on by another SDR team member, please send a Chatter note to Courtland, Chet, Nick, JJ, or Francis as no one else has permission to reassign an SDR on an account.
* In the event that you feel your accounts are non-workable please consult with your SDR Team Lead. If they decide the account isn't workable, we will reassign all accounts and related contacts back to NULL, which will make them available for prospecting by other members of the SDR team.
* We use this [Salesorce report](https://na34.salesforce.com/00O61000003iZLP) to track and ensure proper coverage of Strategic and Large accounts and effectively use the SDR resources.

You should be looking for contacts within your accounts with IT leadership and application development leadership responsibilities. This can be senior technical individual contributors such as software architects, or leaders in the organization's IT department all the way up to a CIO or CTO.

### Compensation<a name="compensation"></a>

* SDR’s compensation is based on two key metrics:
   * Sales Accepted Opportunities (SAOs)
   * Closed won business from SAOs - 1% commission for any closed won opportunity produced, **so as long as the rep is employed as an SDR by GitLab.

Your quota is based solely on SAOs. Think of the 1% commission on opportunities you source that a sales person subsequently closes and wins as an added bonus.

There is an accelerator for SDRs who deliver over their SAO quota. The accelerator only applies to SAOs; the 1% commission on closed won business is not eligible. Your commission is calculated as follows:

* Tier 1 - Base rate: Per accepted opportunity commission is paid at a base rate of your monthly variable on target earnings divided by your monthly target.
* Tier 2 - Accelerator from 100% to 200% achievement: Each accepted opportunity after initial target is achieved will be paid at 1.05 times the base rate.
* Tier 3 - Accelerator from 200% achievment onwards: After 200% quota achievement each opportunity after will be paid at 1.1 times the base rate.
* There is no floor
* Payment tiers are progressive.

[Outbound SDR created opps](https://na34.salesforce.com/00O61000003nmhe) This is the report that GitLab leadership uses to see all the Outbound SDR created opportunities. If you believe an opportunity you sourced is not reflected in our reporting, notify your manager.

### Other measurements<a name="measurements"></a>

SDR's will also be measured on the following:

* Results
  * Pipeline value of SDR sourced opportunities
  * IACV won from opportunities SDR sources
* Activity
  * % of named accounts contacted
  * Number of opportunities created
  * Number of emails sent
  * Number of calls made

Note, while important, the above measurements do not impact your quota attainment. Your main focus will be achieving your SAO quota.

### Other expectations<a name="expectations"></a>

* Attendance to the initial qualfiying meeting scheduled by the SDR is mandatory. The SDR should take notes during the initial qualifying meeting they set up for an AE or SAL.
* It will be in your best interest to sit in on as many meetings as possible at different stages in the buying process to see how the AEs and SALs work with prospects beyond qualifying. The better you understand the AEs and SALs you work with, the better you will be at making them successful through outbound prospecting support.
