---
layout: markdown_page
title: "Revenue Operations - Database Management"
---
<a name="topofpage"></a>
## On this page
{:.no_toc}

- TOC
{:toc}  



## Rules of Engagement   
TO Be defined 


- [Lead Definition](#mql)
    - [A Leads](#aLeads)
    - [Passive Leads](#passiveLeads)
- [Lead Management](#leadManagement)
    - [Glossary](/handbook/revenue-ops)
    - [Segmentation](#segmentation)
    - [Lead Routing](#leadRouting)
    - [Initial Source](#initialSource)
    - [Lead/Contact Status](#statuses)



## Lead Management

### Lead/MQL Definition<a name="mql"></a>
A [Marketo Qualified Lead (MQL)](https://docs.google.com/a/gitlab.com/document/d/1_kSxYGlIZNNyROLda7hieKsrbVtahd-RP6lU6y9gAIk/edit?usp=sharing) is a lead that reaches a certain point threshold (90 pts) based on a demographic, firmographic, and/or behavioral information. Each BDR will be placed into the Marketo queue and will receive a high volume of MQLs to work. Criteria for those leads are set by Marketo and the Marketing Operations team. We have two MQL buckets: (1) hand raisers and (2) passive leads.

### Hand Raisers (A Leads)<a name="aLeads"></a>
Hand raisers are leads who have filled out a [Sales Contact Us](https://about.gitlab.com/sales/) form, signed up for a free [EE trial](https://about.gitlab.com/free-trial/), engaged us through the Web Chat channel, or attended a live Enterprise Edition (EE) Demo. These leads are automatic MQLs regardless of demographic information because they have exhibited clear interest in GitLab’s ability to fulfill a software need.

### Passive Leads<a name="passiveLeads"></a>
Passive leads are leads who have **not** engaged with us in an active manner or completed any of the [Handraiser](#aLeads) actions. For a passive lead to become an MQL, they must meet our minimum person score of 90 pts. A passive lead becomes a MQL via a combination of demographic score and behavior/engagement score.

Because these leads can MQL without showing explicit interest in GitLab paid products, they are routed to Inbound Business Development Representatives to qualify and assess sales-readiness.

### Lead Management<a name="leadManagement"></a>
What leads to qualify, how to do it, and who to send them to. Generally, we follow steps detailed in the [demand waterfall](https://about.gitlab.com/handbook/marketing/marketing-sales-development/#waterfall).

### Segmentation<a name="segmentation"></a>

#### Size
(can be found on the account page in Salesforce)
Strategic = 5000+ users
Large = 751-4999 users
Mid Market = 101-750 users
Small-Medium Business (SMB) = 1-100 users

#### Region/Vertical

##### [APAC](https://docs.google.com/document/d/1Ar0Y49XF0pnvWjhr5-jr0MNKdxfRY2FkS4x9y7KCZw8/edit#)
Asia Pacific
RD = Michael Alessio

##### [EMEA](http://genesisworld.com/assets/uploads/2014/11/map_EMEA.jpg)
Europe, Middle East, and Africa
RD = Richard Pidgeon

##### [US East](https://dev.gitlab.org/gitlab/salesforce/issues/118)
RD = Mark Rogge

##### [US West](https://dev.gitlab.org/gitlab/salesforce/issues/118)
RD = Haydn Mackay

##### Public Sector
Director of Federal Sales = Paul Almeida

### Lead Routing<a name="leadRouting"></a>
#### Contact Requests
Incoming requests that are classified in the `Strategic`, `Large` and `Mid Market` Sales segment, will be routed to the `Strategic Account Leader` or `Account Executive` (aka Account Owner) that owns the account as identified in salesforce.com.
- At the Account Owner's discretion, they may assign lead follow up in these cases to an outbound SDR or inbound BDR or may follow up directly. In any case, follow up must be within **one (1) business day** and tracked as an activity on the record in Salesforce.
- The Account Owner is responsible to ensure communication happens internally and externally within the SLA timeframe.
- The Account Owner is responsible to use the same method of determining `Sales Qualified Amount` so as to not impact our planning in the Revenue Model. This means setting the amount to reflect the number of seats the prospect initially inquiries about, not the amount that will be reflected in the inital order.

All other Contact Requests - `SMB` - will be distributed to the respective Inbound BDR team based on region (EMEA, NCSA, APAC). The contents of the Contact request will determine the follow up next steps:
- Refund: forward the request to AP@. The AP team will handle processing the refund through Zuora & noting the Salesforce record.
- Support: If the inbound BDR can provide a solution to the question please do so. However, if they are a current customer with a Support agreement, it is best to forward to support@.

#### Enterprise Trials (EE Trials)
Free trial requests are routed to the appropriate Inbound BDR team based on region (EMEA, NCSA, APAC). An Enterprise Trial lead is automatically added to a five (5) touch automated nurture in Marketo that runs during the 30-days the trial is active. Additionally the BDR can choose to add the lead to an Outreach sequence that is timed to run in conjunction with the automated Marketo campaign.


### Initial Source<a name="initialSource"></a>
Initial Source is set upon first "known" touch attribution. It should never be changed or overwritten. If merging leads, keep the Initial Source that was created first (if you can tell). If creating a Lead/Contact and you are unsure what Initial Source to use, ask on #Lead-Questions channel in Slack.
- Advertisement
- AE Generated
- CE Download
- CE Usage Ping
- Clearbit
- Conference
- Consultancy Request
- Contact Request
- Datanyze
- Development Request
- DiscoverOrg
- Email Request
- Email Subscription
- Employee Referral
- Enterprise Trial
- Existing Client
- External Referral
- GitLab.com
- GitLab Hosted
- Gitorious
- Leadware
- Legacy
- LinkedIn
- Live Event
- Newsletter
- Other
- Partner
- Prof Serv Request
- Public Relations
- SDR Generated
- Security Newsletter
- Trade Show
- Training Request
- Web
- Webcast
- Web Chat
- Web Direct
- White Paper
- Word Of Mouth


### Lead & Contact Statuses<a name="statuses"></a>
The Lead & Contact objects in Salesforce have unified statuses with the following definitions. If you have questions about current status, please ask in #Lead-Questions channel on Slack.

| Status | Definition |
| :--- | :--- |
| Raw | Untouched brand new lead |
| Inquiry | Form submission, meeting @ trade show, content offer |
| MQL | Marketo Qualified through systematic means |
| Accepted | Actively working to get intouch with the lead/contact |
| Qualifying | In 2-way conversation with lead/contact |
| Qualified | Progressing to next step of sales funnel (typically OPP created & hand off to Sales team) |
| Unqualified | Contact information is not now or ever valid in future; Spam form fill-out |
| Nurture | Record is not ready for our services or buying conversation now, possibly later |
| Bad Data | Incorrect data - to potentially be researched to find correct data to contact by other means |
| Web Portal Purchase | Used when lead/contact completed a purchase through self-serve channel & duplicate record exists |














[Return to Top of Page](#topofpage)