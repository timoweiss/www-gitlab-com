---
layout: job_page
title: "Billing Specialist"
---

We’re looking for a billing specialist that can manage the invoicing and collection process in its entirety. Specifically, we’re looking for someone to orchestrate and own the customer billing cycle from quote to cash. This role will collaborate with the sales team and at times with customers.  Attention to detail and an aptitude for accuracy are critical to being successful.

We are a rapidly growing company which means you must be comfortable working in fast paced atmosphere where juggling numerous priorities and deadlines is the norm. We are also a fully distributed team which means you must be self-driven and a highly effective communicator.

## Responsibilities

- Completely own and manage the billing process
- Work closely with the Sales team on various billing related issues, including quote generation
- Assist in cross-functional accounting activities as needed
- Resolving customer inquiries in an accurate, timely manner
- Identify process and system improvements to streamline the revenue cycle
- Responsible for owning and administering sales commissions using Xactly
- Communicate process improvements by routinely and frequently updating our handbook and training the sales team.
- Financial reporting and analytics of sales figures and A/R data
- Proactively monitor aging reports, following up on delinquent accounts and managing A/R collections
- Provide sales and billing reports to upper management as needed
- Ability to take on side projects related to internal initiatives

## Requirements

- Proven ability to fully utilize the Zuora platform from quote to cash collection
- Experience with Salesforce CRM
- Experience billing in a high-volume environment
- Deep understanding of subscription billing
- Superior attention to detail
- Excellent computer skills, self starter in picking up new and complex systems
- Strong knowledge of Google Apps (Gmail, Docs, Spreadsheets,etc).
- Slack is a plus but not necessary
- Bonus points: experience with Xactly
- Bonus points: experience using NetSuite
- Bonus points: experience working with distributed teams

## Hiring Process

Applicants for this position can expect the hiring process to follow the order below. Please keep in mind that applicants can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find her/his job title on our [team page](https://about.gitlab.com/team/).

- Selected candidates will be invited to schedule a 30 minute screening call with our Global Recruiters
- Next, candidates will be invited to schedule a 45 minute interview with our Controller
- Candidates will then be invited to schedule a 45 minute interview with our CFO
- Finally, candidates may be asked to interview with the CEO
- Successful candidates will subsequently be made an offer via email

Additional details about our process can be found on our [hiring page](https://about.gitlab.com/handbook/hiring/interviewing).

## Apply

Please note that if we are actively hiring for a position, you will see it listed on our [jobs page](https://about.gitlab.com/jobs/), where all of our current openings are advertised. To apply, please click on the name of the role you are interested in, which will take you to our applicant tracking system (ATS), [Lever](https://www.lever.co/).

Avoid the [confidence gap](https://www.theatlantic.com/magazine/archive/2014/05/the-confidence-gap/359815/
); you do not have to match all the listed requirements exactly to apply. Our hiring process is described in more detail in our [hiring handbook](https://about.gitlab.com/handbook/hiring/).

## About GitLab

GitLab Inc. is a company based on the GitLab open-source project. GitLab is a community project to which over 1,000 people worldwide have contributed. We are an active participant in this community, trying to serve its needs and lead by example. We have one [vision](https://about.gitlab.com/strategy/): everyone can contribute to all digital content, and our mission is to change all creative work from read-only to read-write so that everyone can contribute.

We [value](https://about.gitlab.com/handbook/values/) results, transparency, sharing, freedom, efficiency, frugality, collaboration, directness, kindness, diversity, boring solutions, and quirkiness. If these values match your personality, work ethic, and personal goals, we encourage you to visit our [primer](https://about.gitlab.com/primer/) to learn more. Open source is our culture, our way of life, our story, and what makes us truly unique.

Top 10 reasons to work for GitLab:

1. Work with helpful, kind, motivated, and talented people.
1. Work remote so you have no commute and are free to travel and move.
1. Have flexible work hours so you are there for other people and free to plan the day how you like.
1. Everyone works remote, but you don't feel remote. We don't have a head office, so you're not in a satellite office.
1. Work on open source software so you can interact with a large community and can show your work.
1. Work on a product you use every day: we drink our own wine.
1. Work on a product used by lots of people that care about what you do.
1. As a company we contribute more than we take, most of our work is released as the open source GitLab CE.
1. Focused on results, not on long hours, so that you can have a life and don't burn out.
1. Open internal processes: know what you're getting in to and be assured we're thoughtful and effective.

See [our culture page](https://about.gitlab.com/culture) for more!

Work remotely from anywhere in the world. Curious to see what that looks like? Check out our [remote manifesto](https://about.gitlab.com/2015/04/08/the-remote-manifesto/).

