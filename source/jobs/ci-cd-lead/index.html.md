---
layout: markdown_page
title: "CI/CD Lead"
---

The CI/CD Lead reports to the VP of Engineering, and CI/CD specialists report to the
CI/CD Lead.

## Responsibilities

* Continues to spend time on coding
* Interviews applicants for development positions
* Ensures the quality of implementation
* Ensures the simplicity of added features
* Ensures that product design set by the Product managers are being followed
* Does 1:1's with all reports every 2-5 weeks (depending on the experience of the report)
* Is available for 1:1's on demand of the report
* Ensures all tentpole features, promised and bugs are assigned early in the release
* Does weekly stand-up with a team and product managers to talk about plan for the work in the upcoming week
* Coordinates a deployment of CI/CD related services with infrastructure team
* Delivers input on promotions, function changes, demotions and firings in consultation with the CEO, CTO, and VP of Engineering



**NOTE** In the compensation calculator below, fill in "Lead" in the `Level` field for this role.


## Hiring Process

Applicants for this position can expect the hiring process to follow the order below. Please keep in mind that applicants can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find her/his job title on our [team page](https://about.gitlab.com/team/).

- Selected candidates will be invited to schedule a 30min screening call with our Global Recruiters
- Next, candidates will be invited to schedule a 1 hour interview with a CI/CD Engineer or Lead
- Next, candidates may be invited to schedule an interview with either an Engineer or Product Manager
- Candidates will be invited to schedule a interview with our VP of Engineering
- Finally, candidates may be invited to interview with our CEO
- Successful candidates will subsequently be made an offer via email

Additional details about our process can be found on our [hiring page](https://about.gitlab.com/handbook/hiring/interviewing).

## Apply

Please note that if we are actively hiring for a position, you will see it listed on our [jobs page](https://about.gitlab.com/jobs/), where all of our current openings are advertised. To apply, please click on the name of the role you are interested in, which will take you to our applicant tracking system (ATS), [Lever](https://www.lever.co/).

Avoid the [confidence gap](https://www.theatlantic.com/magazine/archive/2014/05/the-confidence-gap/359815/
); you do not have to match all the listed requirements exactly to apply. Our hiring process is described in more detail in our [hiring handbook](https://about.gitlab.com/handbook/hiring/).

## About GitLab

GitLab Inc. is a company based on the GitLab open-source project. GitLab is a community project to which over 1,000 people worldwide have contributed. We are an active participant in this community, trying to serve its needs and lead by example. We have one [vision](https://about.gitlab.com/strategy/): everyone can contribute to all digital content, and our mission is to change all creative work from read-only to read-write so that everyone can contribute.

We [value](https://about.gitlab.com/handbook/values/) results, transparency, sharing, freedom, efficiency, frugality, collaboration, directness, kindness, diversity, boring solutions, and quirkiness. If these values match your personality, work ethic, and personal goals, we encourage you to visit our [primer](https://about.gitlab.com/primer/) to learn more. Open source is our culture, our way of life, our story, and what makes us truly unique.

Top 10 reasons to work for GitLab:

1. Work with helpful, kind, motivated, and talented people.
1. Work remote so you have no commute and are free to travel and move.
1. Have flexible work hours so you are there for other people and free to plan the day how you like.
1. Everyone works remote, but you don't feel remote. We don't have a head office, so you're not in a satellite office.
1. Work on open source software so you can interact with a large community and can show your work.
1. Work on a product you use every day: we drink our own wine.
1. Work on a product used by lots of people that care about what you do.
1. As a company we contribute more than we take, most of our work is released as the open source GitLab CE.
1. Focused on results, not on long hours, so that you can have a life and don't burn out.
1. Open internal processes: know what you're getting in to and be assured we're thoughtful and effective.

See [our culture page](https://about.gitlab.com/culture) for more!

Work remotely from anywhere in the world. Curious to see what that looks like? Check out our [remote manifesto](https://about.gitlab.com/2015/04/08/the-remote-manifesto/).

