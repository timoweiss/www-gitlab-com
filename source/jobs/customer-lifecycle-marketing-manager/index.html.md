---
layout: job_page
title: "Customer Lifecycle Marketing Manager"
---

Are you a passionate marketer with strong program management skills who has mastered the art of building relationships and executing on exciting new projects? GitLab is looking for an awesome Customer Lifecycle Marketing Manager to own the strategy and execution of end-to-end lifecycle marketing initiatives, ranging from first click to retention and renewal initiatives, with a focus on instilling loyalty within the our ever-growing customer base. Our ideal candidate will have no problem rolling up their sleeves and working directly with customers to understand the wants and needs of the market, while ensuring that relevant programs are being created for all stages of the customer journey.

## Responsibilities
* Develop GitLab’s customer lifecycle stages and create strategic initiatives for each stage of the customer journey, which include onboarding, engagement and retention.
* Create and refine effective programs that leverage tenure and customer behavior data, and include a mix of triggered, targeted and broad educational campaigns.
* Identify key, measurable metrics (upsell, churn, etc.) for lifecycle programs encompassing a growth hacker mentality with the ability to test and scale rapidly.
* Forge personal relationships with advocate customers and build out case studies, testimonials and quotes, acting as a content source for GitLab.
* Conceptualize and launch a customer advocacy program and customer community designed to identify and mobilize customers, transforming them into GitLab brand ambassadors.
* Maintain positive and productive relationships with product marketing, product management and engineering teams to ensure messaging of programs and product updates are accurate and meaningful to the developer persona.
* Partner with customer success and account managers to leverage customer interaction touch points and infuse personal, one-to-one programs.
* Participate in GitLab sponsored tradeshows, seminars, workshops and events and build face-to-face loyalty and relationships with prospects and customers.

## Requirements
* 5-7 years experience in customer marketing, preferably at an enterprise technology company.
* A proven track record of delighting customers and creating successful marketing programs aimed at customers.
* Strong communication skills without a fear of over communication. This role will require effective collaboration and cross-functional coordination across internal and external contributors.
* Extremely detail-oriented and organized, able to manage multiple projects to meet deadlines.
* A passion and strong understanding of developer customer personas and the landscape.
* Is your college degree in French foreign politics with a minor in interpretive dance but you’ve been hacking together websites since you were 12? Perfect. We understand that your college degree isn’t the only thing that prepares you as a potential job candidate.
* You share our [values](/handbook/values), and work in accordance with those values.

## Apply

Please note that if we are actively hiring for a position, you will see it listed on our [jobs page](https://about.gitlab.com/jobs/), where all of our current openings are advertised. To apply, please click on the name of the role you are interested in, which will take you to our applicant tracking system (ATS), [Lever](https://www.lever.co/).

Avoid the [confidence gap](https://www.theatlantic.com/magazine/archive/2014/05/the-confidence-gap/359815/
); you do not have to match all the listed requirements exactly to apply. Our hiring process is described in more detail in our [hiring handbook](https://about.gitlab.com/handbook/hiring/).

## About GitLab

GitLab Inc. is a company based on the GitLab open-source project. GitLab is a community project to which over 1,000 people worldwide have contributed. We are an active participant in this community, trying to serve its needs and lead by example. We have one [vision](https://about.gitlab.com/strategy/): everyone can contribute to all digital content, and our mission is to change all creative work from read-only to read-write so that everyone can contribute.

We [value](https://about.gitlab.com/handbook/values/) results, transparency, sharing, freedom, efficiency, frugality, collaboration, directness, kindness, diversity, boring solutions, and quirkiness. If these values match your personality, work ethic, and personal goals, we encourage you to visit our [primer](https://about.gitlab.com/primer/) to learn more. Open source is our culture, our way of life, our story, and what makes us truly unique.

Top 10 reasons to work for GitLab:

1. Work with helpful, kind, motivated, and talented people.
1. Work remote so you have no commute and are free to travel and move.
1. Have flexible work hours so you are there for other people and free to plan the day how you like.
1. Everyone works remote, but you don't feel remote. We don't have a head office, so you're not in a satellite office.
1. Work on open source software so you can interact with a large community and can show your work.
1. Work on a product you use every day: we drink our own wine.
1. Work on a product used by lots of people that care about what you do.
1. As a company we contribute more than we take, most of our work is released as the open source GitLab CE.
1. Focused on results, not on long hours, so that you can have a life and don't burn out.
1. Open internal processes: know what you're getting in to and be assured we're thoughtful and effective.

See [our culture page](https://about.gitlab.com/culture) for more!

Work remotely from anywhere in the world. Curious to see what that looks like? Check out our [remote manifesto](https://about.gitlab.com/2015/04/08/the-remote-manifesto/).


