---
layout: job_page
title: "Backend Developer, Distributed Systems"
---

We are seeking a [Staff level Backend Developer](/jobs/developer/#staff-developer)
with specific expertise in Distributed Systems to join (primarily) in the
development of GitLab Geo. [GitLab Geo](https://docs.gitlab.com/ee/gitlab-geo/README.html)
is an enterprise product feature that speeds up the work of globally distributed
teams, adds redundancy for GitLab instances, and provides Disaster Recovery as
well. As a staff level distributed systems specialist, you would provide deep
technical know-how to the rest of the team, and radiate this knowledge. This
position reports to the [Engineering Manager - Geo](/jobs/engineering-manager-geo).


## Responsibilities

As a Staff Engineer, you are expected to

- Write exquisite code and peer review others’ code
- Ship large features independently
- Be positive and solution oriented
- Make architecture decisions and author technical architecture documents for epics
- Author code tests for hiring process and screen applicants
- Constantly improve the quality & security of the product
- Radiate your knowledge internally and beyond, by writing public blog posts

Within the Geo team specifically, in this role you will

- Architect Geo and Disaster Recovery products for GitLab
- Identify ways to test and improve availability and performance of GitLab Geo at GitLab.com scale
- Instrument and monitor the health of distributed GitLab instances
- Educate all team members on best practices relating to high availability

## Requirements

- Deep experience architecting and implementing fault-tolerant, distributed systems
- Experience building and scaling highly-available systems
- In-depth experience with Ruby on Rails, Go, and/or Git
- Excellent written and verbal communication skills
- You share our [values](/handbook/values), and work in accordance with those values
- [A technical interview](/jobs/#technical-interview) is part of the hiring process for this position.

## Hiring process

The hiring process for this position includes

- A 30min. screening call with one of our Recruiters
- An interview with a Senior Developer with deep knowledge of the Geo feature set
- An interview with the Engineering Manager - Geo
- An interview with the Director of Backend
- An interview with the VP of Engineering


**NOTE** In the compensation calculator below, fill in "Staff" in the `Level` field for this role.

## Apply

Please note that if we are actively hiring for a position, you will see it listed on our [jobs page](https://about.gitlab.com/jobs/), where all of our current openings are advertised. To apply, please click on the name of the role you are interested in, which will take you to our applicant tracking system (ATS), [Lever](https://www.lever.co/).

Avoid the [confidence gap](https://www.theatlantic.com/magazine/archive/2014/05/the-confidence-gap/359815/
); you do not have to match all the listed requirements exactly to apply. Our hiring process is described in more detail in our [hiring handbook](https://about.gitlab.com/handbook/hiring/).

## About GitLab

GitLab Inc. is a company based on the GitLab open-source project. GitLab is a community project to which over 1,000 people worldwide have contributed. We are an active participant in this community, trying to serve its needs and lead by example. We have one [vision](https://about.gitlab.com/strategy/): everyone can contribute to all digital content, and our mission is to change all creative work from read-only to read-write so that everyone can contribute.

We [value](https://about.gitlab.com/handbook/values/) results, transparency, sharing, freedom, efficiency, frugality, collaboration, directness, kindness, diversity, boring solutions, and quirkiness. If these values match your personality, work ethic, and personal goals, we encourage you to visit our [primer](https://about.gitlab.com/primer/) to learn more. Open source is our culture, our way of life, our story, and what makes us truly unique.

Top 10 reasons to work for GitLab:

1. Work with helpful, kind, motivated, and talented people.
1. Work remote so you have no commute and are free to travel and move.
1. Have flexible work hours so you are there for other people and free to plan the day how you like.
1. Everyone works remote, but you don't feel remote. We don't have a head office, so you're not in a satellite office.
1. Work on open source software so you can interact with a large community and can show your work.
1. Work on a product you use every day: we drink our own wine.
1. Work on a product used by lots of people that care about what you do.
1. As a company we contribute more than we take, most of our work is released as the open source GitLab CE.
1. Focused on results, not on long hours, so that you can have a life and don't burn out.
1. Open internal processes: know what you're getting in to and be assured we're thoughtful and effective.

See [our culture page](https://about.gitlab.com/culture) for more!

Work remotely from anywhere in the world. Curious to see what that looks like? Check out our [remote manifesto](https://about.gitlab.com/2015/04/08/the-remote-manifesto/).
