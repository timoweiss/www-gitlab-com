---
layout: job_page
title: "Backend Developer, Edge"
---

Edge Backend Developers are primarily tasked with improving the productivity of
the GitLab developers (from both GitLab Inc and the rest of the community), and
making the GitLab project maintainable in the long-term.

See the description of the [Edge team](/handbook/quality/edge) for more details. Edge
Backend Developers report to the [Edge Lead](/jobs/edge-lead/).

We are currently looking for [backstage specialists](/jobs/specialist/backstage),
or developers with strong interest in this speciality. The position
also involves working with the community as
[merge request coach](/jobs/merge-request-coach), and working together with our
[issue triage specialists](/jobs/specialist/issue-triage) to respond and address
issues from the community.

## Responsibilities

* Take initiative in improving the software in small or large ways to address
  pain points in your own experience as a developer.
* Keep code easy to maintain and keep it easy for others to contribute code to
  GitLab.
* Manage and review code contributed by the rest of the community and work with
  them to get it ready for production.
* Support and collaborate with our [service support](/jobs/support-engineer)
  in getting to the bottom of user-reported issues and come up with robust solutions.
* Engage with the core team and the open source community to collaborate on improving GitLab.
* Qualify developers for hiring.

## Requirements

* You can reason about software, algorithms, and performance from a high level.
* You are passionate about open source.
* You have strong programming skills - Ruby and Rails.
* You know how to write your own Ruby gem using TDD techniques.
* You have worked on a production-level Ruby application, preferably using Rails.
  This is a [strict requirement](#ruby-experience).
* Strong written communication skills.
* You are self-motivated and have strong organizational skills.
* You have an urge to collaborate and communicate asynchronously.
* You have an urge to document all the things so you don't need to learn the same
  thing twice.
* You have an urge to automate all the things so you don't need to do the same thing
  twice.
* You have a proactive, go-for-it attitude. When you see something broken, you can't
  help but fix it.
* You have an urge for delivering quickly and iterating fast.
* Experience with Docker, Nginx, Go, and Linux system administration a plus.
* Experience with online community development a plus.
* You share our [values](/handbook/values), and work in accordance with those
  values.
* [A technical interview](/handbook/hiring/technical) is part of the hiring
  process for this position.

### Ruby experience

For this position, a significant amount of experience with Ruby is a **strict
requirement**.

We would love to hire all great backend developers, regardless of the language
they have most experience with, but at this point we are looking for developers
who can get up and running within the GitLab code base very quickly and without
requiring much training, which limits us to developers with a large amount of
existing experience with Ruby, and preferably Rails too.

For a time, we also considered applicants with little or no Ruby and Rails
experience for this position, because we realize that programming skills are to
a large extent transferable between programming languages, but we are not
currently doing that anymore for the reasons described in the
[merge request](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/2695)
that removed the section from this listing that described that policy.

If you think you would be an asset to our engineering team regardless, please
see if [another position](/jobs) better fits your experiences and interests,
or apply using the [Open Application](/jobs/open-application/).

If you would still prefer to join the backend development team as a Ruby
developer, please consider contributing to the open-source
[GitLab Community Edition](https://gitlab.com/gitlab-org/gitlab-ce). We
frequently hire people from the community who have shown through contributions
that they have the skills that we are looking for, even if they didn’t have much
previous experience with those technologies, and we would gladly review those
contributions.

## Workflow

The basics of GitLab development can be found in the [developer onboarding](/handbook/developer-onboarding/#basics-of-gitlab-development) document.

The handbook details the complete [GitLab Workflow](/handbook/communication/#gitlab-workflow).

## Relevant links

- [Engineering Handbook](/handbook/engineering)
- [Engineering Workflow](/handbook/engineering/workflow)
- [Edge Team](/handbook/quality/edge)
- [Backstage speciality](/jobs/specialist/backstage)
- [Issue Triage speciality](/jobs/specialist/issue-triage)

## Hiring Process

Applicants for this position can expect the hiring process to follow the order below. Please keep in mind that applicants can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find her/his job title on our [team page](/team).


* Part of the application process is to provide links to some code. Some
  candidates want to share code privately, so if they do, we send them an email
  with our GitLab.com / GitHub / Bitbucket usernames and also invite them to
  email code to us.
* If the code looks good, we ask the candidate to answer two questions:
  - One about the GitLab codebase architecture.
  - One about proposing a boring solution about a specific topic the Edge team
  is focusing on.
* Selected candidates will be invited to schedule a 30-minute
  [screening call](/handbook/hiring/#screening-call) with our Global Recruiters.
* The candidate will then be asked to triage / troubleshoot a GitLab issue and
  to review / coach a GitLab merge request (both in a private fork of GitLab CE).
* Next, candidates will be invited to schedule a 45-minute
  [behavioral interview](/handbook/hiring/#behavioral-questions-star) with the
  Edge Lead.
* Candidates will then be invited to schedule a
  [technical interview](/handbook/hiring/technical) with a Senior Developer.
* Candidates will be invited to schedule an interview with our VP of Engineering.
* Finally, candidates will have an interview with our CEO.
* Successful candidates will subsequently be made an offer via email.

Additional details about our process can be found on our [hiring page](/handbook/hiring).

## Apply

Please note that if we are actively hiring for a position, you will see it listed on our [jobs page](https://about.gitlab.com/jobs/), where all of our current openings are advertised. To apply, please click on the name of the role you are interested in, which will take you to our applicant tracking system (ATS), [Lever](https://www.lever.co/).

Avoid the [confidence gap](https://www.theatlantic.com/magazine/archive/2014/05/the-confidence-gap/359815/
); you do not have to match all the listed requirements exactly to apply. Our hiring process is described in more detail in our [hiring handbook](https://about.gitlab.com/handbook/hiring/).

## About GitLab

GitLab Inc. is a company based on the GitLab open-source project. GitLab is a community project to which over 1,000 people worldwide have contributed. We are an active participant in this community, trying to serve its needs and lead by example. We have one [vision](https://about.gitlab.com/strategy/): everyone can contribute to all digital content, and our mission is to change all creative work from read-only to read-write so that everyone can contribute.

We [value](https://about.gitlab.com/handbook/values/) results, transparency, sharing, freedom, efficiency, frugality, collaboration, directness, kindness, diversity, boring solutions, and quirkiness. If these values match your personality, work ethic, and personal goals, we encourage you to visit our [primer](https://about.gitlab.com/primer/) to learn more. Open source is our culture, our way of life, our story, and what makes us truly unique.

Top 10 reasons to work for GitLab:

1. Work with helpful, kind, motivated, and talented people.
1. Work remote so you have no commute and are free to travel and move.
1. Have flexible work hours so you are there for other people and free to plan the day how you like.
1. Everyone works remote, but you don't feel remote. We don't have a head office, so you're not in a satellite office.
1. Work on open source software so you can interact with a large community and can show your work.
1. Work on a product you use every day: we drink our own wine.
1. Work on a product used by lots of people that care about what you do.
1. As a company we contribute more than we take, most of our work is released as the open source GitLab CE.
1. Focused on results, not on long hours, so that you can have a life and don't burn out.
1. Open internal processes: know what you're getting in to and be assured we're thoughtful and effective.

See [our culture page](https://about.gitlab.com/culture) for more!

Work remotely from anywhere in the world. Curious to see what that looks like? Check out our [remote manifesto](https://about.gitlab.com/2015/04/08/the-remote-manifesto/).
