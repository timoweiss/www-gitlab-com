---
layout: job_page
title: "Frontend Engineer"
---

{: .text-center}
<br>

## Responsibilities

* Fix prioritized issues from the issue tracker.
* Write the code to complete scheduled direction issues chosen by our scheduling committee, assigned to you by the frontend lead.
* Continually improve the quality of GitLab by using discretion of where you think changes are needed.
* Create high quality frontend code.
* Frontend Design Specialist: Implement the interfaces in GitLab proposed by UX Engineers and contributors.
* Frontend Design Specialist: Improve the [static website of GitLab](https://about.gitlab.com/) based on the collaborations with the the Designer and CMO.

## Workflow

- You work on issues tagged with 'Frontend' on [CE](https://gitlab.com/gitlab-org/gitlab-ce/issues?label_name=frontend) and [EE](https://gitlab.com/gitlab-org/gitlab-ee/issues?label_name=frontend).
- The priority of the issues tagged with this label can be found in [the handbook under GitLab Workflow](https://about.gitlab.com/handbook/communication/#prioritize).
- When done with a frontend issue remove the 'Frontend' label and add the next [workflow label](https://gitlab.com/gitlab-org/gitlab-ce/blob/master/PROCESS.md#workflow-labels) which is probably the 'Developer' label.

## Requirements for Applicants

* Know how to use CSS effectively
* Expert knowledge of JavaScript
* Basic knowledge of Vue.js is a plus but not a requirement
* Collaborate effectively with UX Designers, Developers, and Designers
* Be able to work with the rest of the community
* Knowledge of Ruby on Rails is a plus
* You share our [values](/handbook/values), and work in accordance with those values.

## Junior Developers
Junior Frontend Developers are developers who meeting the following criteria:
1. Technical skills
  * Needs guidance writing modular and maintainable code
  * Has less experience with HTML, CSS & JavaScript
1. Code quality
  * Leaves code in substantially better shape than before
  * Needs prompting to fix bugs/regressions
1. Communication
  * Needs help to manage time effectively
  * Participates in frontend technical conversations
1. Performance & Scalability
  * Needs help writing production-ready code
  * Has little to no experience writing large scale apps

<%= partial "includes/senior_developer" %>

## Staff Developers
A Senior Developer will be promoted to a Staff Developer when they have
demonstrated significant leadership to deliver high-impact projects. This may
involve any type of consistent "above and beyond senior level" performance,
for example:
1. Technical Skills
    * Identifies significant projects that result in substantial cost savings or revenue
    * Able to create innovative solutions that push GitLab's technical abilities ahead of the curve
2. Leadership
    * Leads the design for medium to large projects with feedback from other engineers
    * Working across functional groups to deliver the project
3. Code quality
    * Proactively identifying and reducing technical debt
    * Proactively defining and solving important architectural issues
4. Communication
    * Writing in-depth documentation that shares knowledge and radiates GitLab technical strengths
5. Performance & Scalability
    * Leads development of projects that lead to a significant improvement of the overall
      performance and scalability of GitLab


## Internships
We normally don't offer any internships, but if you get a couple of merge requests
accepted, we'll interview you for one. This will be a remote internship without
supervision; you'll only get feedback on your merge requests. If you want to
work on open source and qualify please submit an application.
In the cover letter field, please note that you want an internship and link to
the accepted merge requests. The merge requests should be of significant
value and difficulty, which is at the discretion of the manager. For
example, fixing 10 typos isn't as valuable as shipping 2 new features.

## Relevant links

- [Engineering Handbook](/handbook/engineering)
- [Engineering Workflow](/handbook/engineering/workflow)

## Hiring Process

Applicants for this position can expect the hiring process to follow the order below. Please keep in mind that applicants can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find her/his job title on our [team page](/team).

* Qualified applicants receive a short questionnaire from our Global Recruiters
* Selected candidates will be invited to schedule a 45 minute [screening call](/handbook/hiring/#screening-call) with our Global Recruiters
* Next, candidates will be invited to schedule a 45 minute first interview with a Frontend Engineer
* Candidates will then be invited to schedule a 1 hour technical interview with the Frontend Lead
* Candidates will be invited to schedule a third 45 minute interview with our VP of Engineering
* Finally, candidates will schedule a 50 minute interview with our CEO
* Successful candidates will subsequently be made an offer via email

Additional details about our process can be found on our [hiring page](/handbook/hiring).

## Apply

Please note that if we are actively hiring for a position, you will see it listed on our [jobs page](https://about.gitlab.com/jobs/), where all of our current openings are advertised. To apply, please click on the name of the role you are interested in, which will take you to our applicant tracking system (ATS), [Lever](https://www.lever.co/).

Avoid the [confidence gap](https://www.theatlantic.com/magazine/archive/2014/05/the-confidence-gap/359815/
); you do not have to match all the listed requirements exactly to apply. Our hiring process is described in more detail in our [hiring handbook](https://about.gitlab.com/handbook/hiring/).

## About GitLab

GitLab Inc. is a company based on the GitLab open-source project. GitLab is a community project to which over 1,000 people worldwide have contributed. We are an active participant in this community, trying to serve its needs and lead by example. We have one [vision](https://about.gitlab.com/strategy/): everyone can contribute to all digital content, and our mission is to change all creative work from read-only to read-write so that everyone can contribute.

We [value](https://about.gitlab.com/handbook/values/) results, transparency, sharing, freedom, efficiency, frugality, collaboration, directness, kindness, diversity, boring solutions, and quirkiness. If these values match your personality, work ethic, and personal goals, we encourage you to visit our [primer](https://about.gitlab.com/primer/) to learn more. Open source is our culture, our way of life, our story, and what makes us truly unique.

Top 10 reasons to work for GitLab:

1. Work with helpful, kind, motivated, and talented people.
1. Work remote so you have no commute and are free to travel and move.
1. Have flexible work hours so you are there for other people and free to plan the day how you like.
1. Everyone works remote, but you don't feel remote. We don't have a head office, so you're not in a satellite office.
1. Work on open source software so you can interact with a large community and can show your work.
1. Work on a product you use every day: we drink our own wine.
1. Work on a product used by lots of people that care about what you do.
1. As a company we contribute more than we take, most of our work is released as the open source GitLab CE.
1. Focused on results, not on long hours, so that you can have a life and don't burn out.
1. Open internal processes: know what you're getting in to and be assured we're thoughtful and effective.

See [our culture page](https://about.gitlab.com/culture) for more!

Work remotely from anywhere in the world. Curious to see what that looks like? Check out our [remote manifesto](https://about.gitlab.com/2015/04/08/the-remote-manifesto/).
